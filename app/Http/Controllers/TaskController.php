<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Task;
use Exception;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['tasks' => Task::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Task::create([
                'task' => $request->input('task'),
                'completed' => $request->input('completed')
            ]);
        }catch(Exception $e){
            return response()->json(['message' => 'Task No Created'], 200);
        }
        return response()->json(['message' => 'Task Created'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['task' => Task::find($id)], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Task::find($id)->update([
                'task' => $request->input('task'),
                'completed' => $request->input('completed')
            ]);
        }catch(Exception $e){
            return response()->json(['message' => 'Task No Completed'], 200);
        }     
        return response()->json(['message' => 'Task Completed'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {   
        try {
            Task::find($id)->delete();
        }catch(Exception $e){
            return response()->json(['message' => 'Task No Deleted'], 200);
        }    
        return response()->json(['message' => 'Task Deleted'], 200);
    }
}
